package tam;

import org.json.JSONArray;
import org.json.JSONObject;


public class Pattern {
    int numberOfAnswer;
    JSONArray data;
    String[] answer;
    public static int maxNumberOfAnswer = 100;
    Pattern(){
        data = new JSONArray();
        numberOfAnswer = 0;
        answer = new String[Pattern.maxNumberOfAnswer];
    }
    
    private void resetAnswerArray(){
        for (String s: answer){
            s = null;
        }
    }
    public void makeAnswerArray(int i, String uncompleteAnswer){
        this.resetAnswerArray();
        if (i == data.length() - 1){
            if (!data.get(i).equals("")){
                uncompleteAnswer += data.get(i);
            }
            answer[numberOfAnswer] = uncompleteAnswer;
            numberOfAnswer++;
            return;            
        }
        if (data.get(i) instanceof String){
            if (!data.get(i).equals("")){
                uncompleteAnswer += data.get(i);
                uncompleteAnswer += " ";
            }
            makeAnswerArray(i + 1, uncompleteAnswer);
        }
        else{
            for (int j = 0; j < data.getJSONArray(i).length(); j++){
                if (!data.getJSONArray(i).get(j).equals("")){
                    makeAnswerArray(i + 1, uncompleteAnswer + data.getJSONArray(i).get(j) + " ");
                }
                else{
                    makeAnswerArray(i + 1, uncompleteAnswer);
                }
            }
        }
    }
    
    public boolean checkAnswer (String answerInput){
        for (int i = 0; i < numberOfAnswer; i++){
            if (this.answer[i].equals(answerInput)){
                return true;
            }
        }
        
        //If input answer have just only one wrong word, try to find it.
        for (int i = 0; i < numberOfAnswer; i++){
            /*
            j: first char of first different word
            k: last char of last different exact answer
            l: last char of last different input answer
            */
            int j, k, l;
            j = 0;
            k = this.answer[i].length();
            l = answerInput.length();            
            if (k >= l){
                while (this.answer[i].charAt(j) == answerInput.charAt(j)){
                    j++;
                    if (j == l){
                        break;
                    }
                }
                while (j > 0){
                    if (this.answer[i].charAt(j - 1) != ' '){
                        j--;
                    }
                    else{
                        break;
                    }
                }
                do{
                    if (l == j){
                        break;
                    }
                    k--;
                    l--;
                } while (this.answer[i].charAt(k) == answerInput.charAt(l));
                while (k + 1 < this.answer[i].length()){
                    if (this.answer[i].charAt(k + 1) != ' '){
                        k++;
                        l++;
                    }
                    else{
                        break;
                    }                    
                }
                String diffirent1 = this.answer[i].substring(j, k + 1);
                String diffirent2 = answerInput.substring(j, l + 1);
                if (diffirent2.equals("")){
                    break;
                }
                int check = 0;
                for (int index = 0; index < diffirent1.length(); index++){
                    if (diffirent1.charAt(index) == ' '){
                        check = 1;
                        break;
                    }
                }
                for (int index = 0; index < diffirent2.length(); index++){
                    if (diffirent2.charAt(index) == ' '){
                        check = 1;
                        break;
                    }
                }
                if (check == 0){
                    System.out.println ("\"" + diffirent2 + "\" is the wrong word.");
                    //System.out.println ("False trong nhanh " + i);
                    return false;
                }
            }
            else{
                while (this.answer[i].charAt(j) == answerInput.charAt(j)){
                    j++;
                    if (j == k){
                        break;
                    }
                }
                while (j > 0){
                    if (this.answer[i].charAt(j - 1) != ' '){
                        j--;
                    }
                    else{
                        break;
                    }
                }
                do{
                    if (k == j){
                        break;
                    }
                    k--;
                    l--;
                } while (this.answer[i].charAt(k) == answerInput.charAt(l));
                while (k + 1 < this.answer[i].length()){
                    if (this.answer[i].charAt(k + 1) != ' '){
                        k++;
                        l++;
                    }
                    else{
                        break;
                    }                    
                }
                String diffirent1 = this.answer[i].substring(j, k + 1);
                String diffirent2 = answerInput.substring(j, l + 1);
                if (diffirent2.equals("")){
                    break;
                }
                int check = 0;
                for (int index = 0; index < diffirent1.length(); index++){
                    if (diffirent1.charAt(index) == ' '){
                        check = 1;
                        break;
                    }
                }
                for (int index = 0; index < diffirent2.length(); index++){
                    if (diffirent2.charAt(index) == ' '){
                        check = 1;
                        break;
                    }
                }
                if (check == 0){
                    System.out.println ("\"" + diffirent2 + "\" is the wrong word.");
                    //System.out.println ("False trong nhanh" + i);
                    return false;
                }
            }
        }
        //System.out.println ("False cuoi cung");
        return false;
    }
    
    public void printAnswerArray(){
        for (String s: answer){
            if (s == null){
                break;
            }
            System.out.println (s);
        }
    }
}
