package tam;

import org.junit.*;
import static org.junit.Assert.*;
import org.json.JSONArray;

public class PatternTest{
    @Test
    public void testPrintAnswerNonUTF8(){
        Pattern pattern = new Pattern();
        JSONArray subPattern1 = new JSONArray();
        subPattern1.put("Nhung");
        subPattern1.put("Cac");
        JSONArray subPattern2 = new JSONArray();
        subPattern2.put("nhung");
        subPattern2.put("cac");
        JSONArray subPattern3 = new JSONArray();
        subPattern3.put("nen");
        subPattern3.put("");
        pattern.data.put(subPattern1);
        pattern.data.put("nen van minh khac nhau co");
        pattern.data.put(subPattern2);
        pattern.data.put(subPattern3);
        pattern.data.put("van hoa khac nhau");
        pattern.makeAnswerArray(0, "");
        pattern.printAnswerArray();
    }
    
    @Test
    public void testPrintAnswerUTF8(){
        Pattern pattern = new Pattern();
        JSONArray subPattern1 = new JSONArray();
        subPattern1.put("Những");
        subPattern1.put("Các");
        JSONArray subPattern2 = new JSONArray();
        subPattern2.put("những");
        subPattern2.put("các");
        JSONArray subPattern3 = new JSONArray();
        subPattern3.put("nền");
        subPattern3.put("");
        pattern.data.put(subPattern1);
        pattern.data.put("nền văn minh khác nhau có");
        pattern.data.put(subPattern2);
        pattern.data.put(subPattern3);
        pattern.data.put("văn hoá khác nhau");
        pattern.makeAnswerArray(0, "");
        pattern.printAnswerArray();
    }
    
    @Test
    public void testCheckRightAnswerNonUTF8(){
        Pattern pattern = new Pattern();
        JSONArray subPattern1 = new JSONArray();
        subPattern1.put("Nhung");
        subPattern1.put("Cac");
        JSONArray subPattern2 = new JSONArray();
        subPattern2.put("nhung");
        subPattern2.put("cac");
        JSONArray subPattern3 = new JSONArray();
        subPattern3.put("nen");
        subPattern3.put("");
        pattern.data.put(subPattern1);
        pattern.data.put("nen van minh khac nhau co");
        pattern.data.put(subPattern2);
        pattern.data.put(subPattern3);
        pattern.data.put("van hoa khac nhau");
        pattern.makeAnswerArray(0, "");
        assertEquals (true, pattern.checkAnswer("Nhung nen van minh khac nhau co nhung van hoa khac nhau"));
        assertEquals (true, pattern.checkAnswer("Cac nen van minh khac nhau co nhung nen van hoa khac nhau")); 
        assertEquals (true, pattern.checkAnswer("Cac nen van minh khac nhau co nhung van hoa khac nhau")); 
    }
    
    @Test
    public void testCheckRightAnswerUTF8(){
        Pattern pattern = new Pattern();
        JSONArray subPattern1 = new JSONArray();
        subPattern1.put("Những");
        subPattern1.put("Các");
        JSONArray subPattern2 = new JSONArray();
        subPattern2.put("những");
        subPattern2.put("các");
        JSONArray subPattern3 = new JSONArray();
        subPattern3.put("nền");
        subPattern3.put("");
        pattern.data.put(subPattern1);
        pattern.data.put("nền văn minh khác nhau có");
        pattern.data.put(subPattern2);
        pattern.data.put(subPattern3);
        pattern.data.put("văn hoá khác nhau");
        pattern.makeAnswerArray(0, "");
        assertEquals (true, pattern.checkAnswer("Những nền văn minh khác nhau có những văn hoá khác nhau"));
        assertEquals (true, pattern.checkAnswer("Những nền văn minh khác nhau có các nền văn hoá khác nhau")); 
        assertEquals (true, pattern.checkAnswer("Các nền văn minh khác nhau có những văn hoá khác nhau")); 
    }
    
    
    @Test
    public void testCheckWrongAnswer(){
        Pattern pattern = new Pattern();
        JSONArray subPattern1 = new JSONArray();
        subPattern1.put("Nhung");
        subPattern1.put("Cac");
        JSONArray subPattern2 = new JSONArray();
        subPattern2.put("nhung");
        subPattern2.put("cac");
        JSONArray subPattern3 = new JSONArray();
        subPattern3.put("nen");
        subPattern3.put("");
        pattern.data.put(subPattern1);
        pattern.data.put("nen van minh khac nhau co");
        pattern.data.put(subPattern2);
        pattern.data.put(subPattern3);
        pattern.data.put("van hoa khac nhau");
        pattern.makeAnswerArray(0, "");
        assertEquals (false, pattern.checkAnswer("Cac nen van minh khac nhau co nhung nen van hoa gion nhau"));
        assertEquals (false, pattern.checkAnswer("Cac nen van minh khac nhau co nhung nen van hoa vo cung khac nhau"));
        assertEquals (false, pattern.checkAnswer("khac nhau co nhung nen van hoa gion nhau"));
    }
    
    @Test
    public void testCheckWrongAnswerUTF8(){
        Pattern pattern = new Pattern();
        JSONArray subPattern1 = new JSONArray();
        subPattern1.put("Những");
        subPattern1.put("Các");
        JSONArray subPattern2 = new JSONArray();
        subPattern2.put("những");
        subPattern2.put("các");
        JSONArray subPattern3 = new JSONArray();
        subPattern3.put("nền");
        subPattern3.put("");
        pattern.data.put(subPattern1);
        pattern.data.put("nền văn minh khác nhau có");
        pattern.data.put(subPattern2);
        pattern.data.put(subPattern3);
        pattern.data.put("văn hoá khác nhau");
        pattern.makeAnswerArray(0, "");
        assertEquals (false, pattern.checkAnswer("Những nền văn minh khác nhau có những văn hoá khac nhau"));
        assertEquals (false, pattern.checkAnswer("Những nền văn minh khác nhâu có các nền văn hoá khác nhau")); 
        assertEquals (false, pattern.checkAnswer("Các nền vân minh khác nhau có những văn hoá khác nhau")); 
    }
}